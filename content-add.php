<?php
// 
#==============================================
# content-add.php
#
# The default template for displaying additional content
#==============================================
// 


  $custom_fields = get_post_custom( get_the_ID() );
  $video = $custom_fields['youtube_videos'];
?>

<div id="post-<?php the_ID(); ?>">
  <a class="youtube" href="<?php echo $video[0]?>">
      <div class="clip_wrapper"> 
        <div class="play-button">
            <div class="normal"></div>
            <div class="inverted"></div>
        </div>
          <div class="third-effect">
              <?php if ( has_post_thumbnail() ) : ?>
                  <?php the_post_thumbnail('full', array('class' => 'img-responsive')) ?>
              <?php endif; ?>
          </div>
        <div class="name">
            <?php 
          echo '<h4>' . get_the_title() . ' </h4>'
          ?>
        </div>
      </div>
  </a>    
</div> 
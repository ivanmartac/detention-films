<?php
// 
#======================================
# functions.php
#
# The themes functions
#======================================
// 

// ======================================
# 1.	CONSTANTS
// ======================================

define('THEMEROOT', get_stylesheet_directory_uri() );
define('IMAGES', THEMEROOT . '/images' );
define('JS', THEMEROOT . '/js' );

// ======================================
# 2. THEME SETUP
// ======================================

	if(! function_exists('ivan_theme_setup')) {
		function ivan_theme_setup(){

			// Make theme available for translation

			$lang_dir = THEMEROOT . '/languages';
			load_theme_textdomain ('ivan', $lang_dir);

			// Add support for automatic feed links

			add_theme_support('automatic-feed-links');

			// Add support for automatic feed links

			add_theme_support('post-thumbnails');

			// Register nav menus

			register_nav_menus( array(
				'main_menu' => __('Main menu', 'ivan')
				
			));
		}

		add_action('after_setup_theme', 'ivan_theme_setup');
	}


// ======================================
# 3. SCRIPTS
// ======================================

if ( ! function_exists('ivan_scripts') ) {
	function ivan_scripts() {
		// Register scripts

		wp_register_script('jquery-js', JS . '/jquery.min.js', false, false, true);
		wp_register_script('bootstrap-js', JS . '/bootstrap.min.js', array('jquery'), false, true);
		wp_register_script('colorbox-js', JS . '/jquery.colorbox-min.js', array('jquery'), false, true);
		wp_register_script('easing-js', JS . '/jquery.easing.min.js', array('jquery'), false, true);
		wp_register_script('scrollto-js', JS . '/jquery-scrollto.js', array('jquery'), false, true);
		wp_register_script('slick-js', JS . '/slick.js', array('jquery'), false, true);
		wp_register_script('validate-js', JS . '/jquery.validate.js', array('jquery'), false, true);
		wp_register_script('main-js', JS . '/main.js', false, false, true);

		// Load the custom script

		wp_enqueue_script('jquery-js');
		wp_enqueue_script('bootstrap-js');
		wp_enqueue_script('colorbox-js');
		wp_enqueue_script('easing-js');
		wp_enqueue_script('scrollto-js');
		wp_enqueue_script('slick-js');
		wp_enqueue_script('validate-js');
		wp_enqueue_script('main-js');

		// Load the stylesheets

		wp_enqueue_style('bootstrap-css', THEMEROOT . '/css/bootstrap.min.css');
		wp_enqueue_style('colorbox-css', THEMEROOT . '/css/colorbox.css');
		wp_enqueue_style('slick-css', THEMEROOT . '/css/slick.css');	
		wp_enqueue_style('single-css', THEMEROOT . '/css/single.css');
		wp_enqueue_style('style-css', THEMEROOT . '/css/style.css');

	}

	add_action('wp_enqueue_scripts','ivan_scripts' );
}

// Add new post image size


if ( ! function_exists( 'add_theme_support' ) ) {

	add_theme_support( 'post-thumbnails' ); // enable feature

	set_post_thumbnail_size( 96, 96, true ); // default size

	add_image_size( 'custom-post-image', 784, 384, true ); // custom size

}


?>
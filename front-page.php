<?php
// 
#======================================
# template-home.php
#
# Template name: Homepage
#======================================
// 
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php wp_title('|', true, 'right'); ?><?php bloginfo('name'); ?></title>

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

	<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>
	<div id="fb-root"></div>
	  


	<div class="main_area">
		<div class="header_area"><!-- Header_area -->
			<div id="home" class="header_area_wrapper">
				<nav id="navigation">
				    <?php
				    	wp_nav_menu(array(
				    		'menu_class' => 'main_nav',
				    		'theme_location' => 'main_menu',
				    		'container' => false
				    	));
				    ?>
				</nav>
	    		<div id="home" class="header_section">
					<div class="logo_section">
						<a href="#" class="logo">Detention films</a>
					</div>	
					<div class="show_recent_content">
						<div class="show_recent_content_button">
							<a class="scroll-to" href="#content"><span>View recent contnet</span></a>
						</div>			
					</div>
				</div>
			</div>
		</div><!-- header_area_end -->


		<div id="content" class="recent_content_area"><!-- recent_content_area -->
			<div class="recent_content_area_wrapper">
				<h2><u>Recent content</u></h2>
				<h3>Feature films</h3>
				<div class="recent_films">
					<div class="responsive"> 
			            <?php if (have_posts() ) :

						query_posts( 'cat=1&oderby=id&order=ASC' );
			             while( have_posts() ) : the_post(); ?>
			            	<?php get_template_part('content', get_post_format() ); ?>
			            <?php endwhile; ?>
			        <?php else : ?>
			        	<?php get_template_part('content', 'none'); ?>
			        <?php endif; ?>
		        	</div>    
				</div>

				<h3>Additional content</h3>
				<div class="additional_content">
					<div class="responsive">
								<?php if (have_posts() ) : 
								query_posts( 'cat=5&oderby=id&order=ASC'  );
								while( have_posts() ) : the_post(); ?>
									<?php get_template_part('content-add', get_post_format() ); ?>
					            <?php endwhile; ?>
					        <?php else : ?>
					        	<?php get_template_part('content-add', 'none'); ?>
					        <?php endif; ?>
				    </div>
				</div>
			        
			</div>
		</div><!-- recent_content_area_end -->

		<div id="about" class="about_area">
		<?php 
		$aboutimage = IMAGES . '/Uneasy_2c.jpg';
		?>
			<div class="about_area_wrapper">
				<h2><u>About detention films</u></h2>
				<div class="about_section">
					<div class="container">
						<div class="row">
							<div class="col-md-5">
								<img class="about_img" src="<?php echo $aboutimage; ?>"/>
							</div>
							<div class="col-md-7">
								<div class="about_films">
									<p>Detention Films is a Los Angeles based production company that thrives in visual storytelling.  From feature films to 30-second commercials, we know what it takes to create sophisticated, high quality entertainment that engages our audiences through out the world.</p>
									<p>With nearly a decade of experience, Detention Films is able to walk the fine line between art and commerce by telling heart felt stories with powerful imagery, strong performances, and unbeatable production value.</p>
								</div>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
		<div id="contact" class="contact_area">
			<div class="contact_area_wrapper">
				<h2><u>Contact us</u></h2>
				<div class="contact_wrapper">
					<form action="<?php echo get_template_directory_uri() . '/test.php'; ?>" id="js-contact-us-form"  method="post">
						<div class="form_content_name">
							<input class="contact_name required" type="text" name="contact-form-name" id="js-contact-form-name"  placeholder="name">
						</div>
						<div class="form_content_email">	
							<input class="contact_email email required" type="email" name="contact-form-email" id="js-contact-form-email" placeholder="email">
						</div>
						<div class="form_content_text">
							<textarea class="contact_text required" name="contact-form-message" id="js-contact-form-message" placeholder="message"></textarea>
						</div>
						<div class="pull-left" id="contact_form_message"></div>
						<div class="submit_wrapper">
							<button id="js-submit-contact-form" class="right" type="submit">
							    <h2 class="underlined"><u>
							        <span id="send-message">Send Message</span>
								</u></h2>
						    </button>
						</div>
					</form>	
				</div>
				<div id="js-contact-form-status" class="pull-left"></div>
				
			</div>
			<div class="clear"></div>
		</div>

		<?php
		// Load footer

		get_footer();

		?>
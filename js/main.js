// Preloader
  //<![CDATA[
        $(window).load(function() { // makes sure the whole site is loaded
            $('#status').fadeOut(); // will first fade out the loading animation
            $('#preloader').delay(650).fadeOut('slow'); // will fade out the white DIV that covers the website.
            $('body').delay(650).css({'overflow':'visible'});
        })
    //]]>

// // Add particular classes to a tags in navigation

// Main menu

		
	$('ul.main_nav').children('li:nth-child(1)').children('a').addClass('home scroll-to');
	$('ul.main_nav').children('li:nth-child(2)').children('a').addClass('content scroll-to');
	$('ul.main_nav').children('li:nth-child(3)').children('a').addClass('about scroll-to');
	$('ul.main_nav').children('li:nth-child(4)').children('a').addClass('contact scroll-to');



// Single page menu

	$('ul.single_main_nav').children('li:nth-child(1)').children('a').attr('href', 'http://ivanmartac.comxa.com/wordpress/#home');
	$('ul.single_main_nav').children('li:nth-child(2)').children('a').attr('href', 'http://ivanmartac.comxa.com/wordpress/#content');
	$('ul.single_main_nav').children('li:nth-child(3)').children('a').attr('href', 'http://ivanmartac.comxa.com/wordpress/#about');
	$('ul.single_main_nav').children('li:nth-child(4)').children('a').attr('href', 'http://ivanmartac.comxa.com/wordpress/#contact');







// Hover navigation
		$(window).scroll(function() {

            if ($(window).scrollTop() > 40) {
                $('#navigation').addClass('hover');
            } else {
                $('#navigation').removeClass('hover');
            }
        });

        $(window).scroll(function() {

            if ($(window).scrollTop() > 40) {
                $('.main_nav').removeClass('main_nav').addClass('main_nav_hover');
            } else {
                $('.main_nav_hover').removeClass('main_nav_hover').addClass('main_nav');
            }
        });

// Carousel-slick

$('.responsive').slick({
	  dots: false,
	  infinite: true,
	  speed: 600,
	  slidesToShow: 4,
	  slidesToScroll: 1,
	  responsive: [
		{
		  breakpoint: 1024,
		  settings: {
			slidesToShow: 4,
			slidesToScroll: 1,
			infinite: true,
			dots: true
		  }
		},
		{
		  breakpoint: 768,
		  settings: {
			slidesToShow: 4,
			slidesToScroll: 4,
			infinite: true,
			dots: true
		  }
		},
		{
		  breakpoint: 600,
		  settings: {
			slidesToShow: 4,
			slidesToScroll: 4
		  }
		},
		{
		  breakpoint: 480,
		  settings: {
			slidesToShow: 1,
			slidesToScroll: 1
		  }
		}
	  ]
	});	

 // Scroll to particular content
function scrollToDiv(hash) {
	$(hash).ScrollTo({
		duration: 600,
		easing: 'easeOutCubic'
	});
}

 $(document).ready(function() {

	var hash = window.location.hash;

	if(hash) { 
		scroll(0,0); 
		// cross-browser solution.
		setTimeout( function() { 
			scroll(0,0); 
		}, 1);

		setTimeout(function() { 
			scrollToDiv(hash) 
		}, 1200);
	}



	$('a.scroll-to').on('click', function(){
		var hash = $(this).attr('href');

		scrollToDiv(hash);
	});

});




 // jwplayer_content_and coolorbox


 $('.arrow').on({
  mouseenter: function() {
    $(this).find('.normal').stop(true, true).fadeOut();
    $(this).find('.inverted').stop(true, true).fadeIn();
  },
  mouseleave: function() {
    $(this).find('.normal').stop(true, true).fadeIn();
    $(this).find('.inverted').stop(true, true).fadeOut();
  }
});

$('.play-button').on({
  mouseenter: function() {
    $(this).find('.inverted').stop(true, true).fadeIn();
  },
  mouseleave: function() {
    $(this).find('.inverted').stop(true, true).fadeOut();
  }
});

$('.movies-carousel li, .image-gallery-single-image, .about-detention-films-video, .video-gallery a, a.feature-image-container-with-video').on({
  mouseenter: function() {
    $(this).find('.black-overlay').stop(true, true).fadeIn();
  },
  mouseleave: function() {
    $(this).find('.black-overlay').stop(true, true).fadeOut();
  }
});



$('body').on({
  mouseenter: function() {
    $(this).stop(true, true).animate({
      opacity: 1
    });
  },
  mouseleave: function() {
    $(this).stop(true, true).animate({
      opacity: 0.5
    });
  }
}, '#cboxClose');


$('.image-gallery a').colorbox({
  rel: 'image-gallery-single-image',
  maxHeight: '80%'
});

$('.feature-image-container-with-no-video').colorbox({
  maxHeight: '80%'
});

 $('a.youtube').colorbox({iframe: true, width: 640, height: 390, href:function(){
        var videoId = new RegExp('[\\?&]v=([^&#]*)').exec(this.href);
        if (videoId && videoId[1]) {
            return 'http://youtube.com/embed/'+videoId[1]+'?rel=0&wmode=transparent';
        }
    }});


// Contact form

$(document).ready(function() {
$("#js-contact-us-form").validate({
		errorElement: "p",
		submitHandler: function(){
			$("#contact_form_message").html("SENDING EMAIL. PLEASE WAIT...");
			var dataString = $("form#js-contact-us-form").serialize();
			$.ajax({
			type: "POST",
			url: $('#js-contact-us-form').attr('action'),
			cache: false,
			data: dataString,
			success:function(data)
			{	
			if(data==1)
				{	 
					$("#contact_form_message").slideDown(500);
					$("#contact_form_message").html("EMAIL SENT SUCCESSFULLY.");
					//document.getElementById("ContactSuccessMessage").style.color='#FF0000';
					$("#js-contact-us-form")[0].reset();
					return false;
				}
				else
				{
					$("#contact_form_message").slideDown(500);
					$("#contact_form_message").html("SORRY WE COULDN'T SEND YOUR EMAIL.PLEASE TRY LATER.");
					//document.getElementById("ContactErrorMessage").style.color='#FF0000';
					$("#js-contact-us-form")[0].reset();
					return false;
				}
			},
			
		}); 
		return false;
		}
	});

});






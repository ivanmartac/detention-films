<?php
// 
#==============================================
# footer.php
#
# The template for loading the footer.
#==============================================
// 
 
?>

		<div class="footer_area">
			<div class="footer_area_wrapper">
				<div class="copyright">
		         	<p>&copy; Copyright 2013 Dentention Films</p>
		        </div>
		        <div class="like_wrapper">
		        	<div class="facebook_like">
			         	<iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FDetentionFilms&amp;send=false&amp;layout=button_count&amp;width=450&amp;show_faces=false&amp;font&amp;colorscheme=light&amp;action=like&amp;height=21&amp;appId=640899279268843" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:75px; height:21px;" allowTransparency="true"></iframe>
			        </div>
			        <div class="like">
			        	<p>Like Detention Films on Facebook</p>
			        </div>
		        </div>
		        <div class="clear"></div>
			</div>
		</div>

		<?php wp_footer(); ?>

	</div>
</body>
</html>
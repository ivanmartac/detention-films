<?php
// 
#======================================
# index.php
#
# The main php index.php fail
#======================================
// 

?>

<?php
// Load header.php

get_header();

?>

<?php

	// getting required data. 

	// main image
	$customFields = get_post_custom( get_the_ID() );
  	$mainImage = $customFields['main_image'];

  	// buy links
  	$iTunes = $customFields['itunes'];
  	$hulu = $customFields['hulu'];
  	$amazon = $customFields['amazon'];

  	// links
  	$ThanksKilling3 = $customFields['ThanksKilling3'];
  	$AstheFalconFlies = $customFields['AstheFalconFlies'];
  	$ThatSideOfShadow = $customFields['ThatSideOfShadow'];
  	$AndUneasyLiesTheMind = $customFields['AndUneasyLiesTheMind'];
  	$AndUneasyLiesTheMind2 = $customFields['AndUneasyLiesTheMind2'];

  	// gallery
  	$thumbnails = $customFields['small_gallery'];
  	$originalImages = $customFields['big_gallery'];

  	// post videos
  	$videos_img = $customFields['youtube_img'];
  	$videos = $customFields['youtube_videos'];

  	// post url
  	$url = $customFields['post_url'];
  	


?>

<div class="main_content">
		<div id="fb-root"></div>
			<script>(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=640899279268843";
			fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));</script>
       <?php if (have_posts() ) : while( have_posts() ) : the_post(); ?>
		<div class="movie_area">
			<div class="movie_area_wrapper">
				<aside class="main_image">
					<?php if ($mainImage) : ?>
						<a href="<?php echo $mainImage[0] ?>" class="feature-image-container feature-image-container-with-no-video js-no-opacity-change">
							<img class="feature-image" src="<?php echo $mainImage[0] ?>" alt="<?php the_title(); ?>" />
						</a>
  					<?php endif; ?>
					<h2>
					<?php if($url): ?>
          					<?php echo '<strong> IN DEVELOPMENT </strong>' ?>
          			<?php else: ?>
					Buy / Watch now:
					<?php endif; ?>
					</h2>
					<?php if($iTunes): ?>
						<a href="<?php echo $iTunes[0]?>" target="_blank">iTunes</a> 
					<?php endif; ?>
					<?php if($hulu): ?>
						• 
						<a href="<?php echo $hulu[0]?>" target="_blank">Hulu </a> 
					<?php endif; ?>
					<?php if($amazon): ?>
						• 
						<a href="<?php echo $amazon[0]?>" target="_blank">Amazon</a> 
					<?php endif; ?>

				</aside>
				<aside class="movie_describe">
					<h1><?php the_title(); ?></h1>
					<div class="content">
						<?php the_content(); ?>
					</div>
				    <hr>
				    <div class="social_media">
			            <div class="pull-left facebook-share">
			              <div class="fb-like" data-href="http://detentionfilms.com/movie/uneasy-lies-the-mind/" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false"></div>
			            </div>

			            <div class="pull-left twitter-share">
			              <a href="https://twitter.com/share" class="twitter-share-button" data-text="And Uneasy Lies The Mind - http://detentionfilms.com/wp-content/uploads/2013/06/AndUneasy_Poster.jpg">Tweet</a>
			              <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
			            </div>

			            <div class="pull-left pinterest-share">
			              <a href="//pinterest.com/pin/create/button/?url=http%3A%2F%2Fdetentionfilms.com%2Fmovie%2Funeasy-lies-the-mind%2F&amp;media=http%3A%2F%2Fdetentionfilms.com%2Fwp-content%2Fuploads%2F2013%2F06%2FAndUneasy_Poster.jpg&amp;description=And+Uneasy+Lies+The+Mind" data-pin-do="buttonPin" data-pin-config="beside"><img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" /></a>
			              <script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
			            </div>
          			</div>

          			<ul class="video-gallery unstyled clearfix">
          				<?php if($url): ?>
          					<?php echo 'IN DEVELOPMENT' ?>
          				<?php else: ?>
						<?php 
                    	$videoDetails = explode(',', $videos_img[0]);
                    	$videos_gallery = explode(',', $videos[0]);
                    	foreach($videoDetails as $key => $details): ?>
                    	<?php $splittedVideoData = explode('|', $details);?>
		                <li>
		                  <a class="youtube" href="<?php echo $videos_gallery[$key]; ?>">
		                   	<div class="play-button">
			                    <div class="normal"></div>
			                    <div class="inverted"></div>
		                    </div>
							<div class="black-overlay"></div>
							<img class="video_img" src="<?php echo $splittedVideoData[0]; ?>" alt="#">
							<h3 class="movie-name"><?php echo $splittedVideoData[1]; ?></h3>
		                  </a>
		                  <?php endforeach; ?>
		                  <?php endif; ?>
		                </li>
                    </ul>
          
                    <ul class="image-gallery unstyled clearfix">
                    	<?php if($url): ?>
          					<?php echo '' ?>
          				<?php else: ?>
                    	<?php 
                    	$thumbnails = explode(',', $thumbnails[0]);
                    	$originals = explode(',', $originalImages[0]);
                    	foreach($thumbnails as $key => $thumbnail): ?>
                          <li>
			                <a class="image-gallery-single-image" href="<?php echo $originals[$key]; ?>">
			                  <div class="black-overlay"></div>
							  <img src="<?php echo $thumbnail; ?>" alt="">
			                </a>
			              </li>
			            <?php endforeach; ?>
			            <?php endif; ?>
			        </ul>
			        <div class="single-movie-pagination">
			        <?php if($AstheFalconFlies): ?>
						<a href="<?php echo $AstheFalconFlies[0]?>">&laquo; As the Falcon Flies</a> 
														/
						<a href="<?php echo $ThanksKilling3[0]?>"> Thanksilling3 &raquo;</a>
					<?php endif; ?>
					<?php if($ThatSideOfShadow): ?>
						<a href="<?php echo $ThatSideOfShadow[0]?>">&laquo; That Side Of a Shadow</a> 
					<?php endif; ?>
					<?php if($AndUneasyLiesTheMind): ?>
						<a href="<?php echo $AndUneasyLiesTheMind[0]?>">&laquo; And Uneasy Lies The Mind</a> 
														/
						<a href="<?php echo $ThanksKilling3[0]?>"> Thanksilling3 &raquo;</a>
					<?php endif; ?>
					<?php if($AndUneasyLiesTheMind2): ?>
						<a href="<?php echo $AndUneasyLiesTheMind2[0]?>">&laquo; And Uneasy Lies The Mind</a> 
					<?php endif; ?>
                    </div>
			    </aside>
			</div>
		</div>

<?php endwhile; ?>

<?php else : ?>
	<?php _e( 'Ooops it seems that is nothing here', 'ivan'); ?>
<?php endif; ?>
				

	
			

<?php
// Load footer

get_footer();

?>
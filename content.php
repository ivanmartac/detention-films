<?php
// 
#==============================================
# content.php
#
# The default template for displaying content
#==============================================
// 

?>

<div id="post-<?php the_ID(); ?>"> 
  <div class="third-effect">
  <?php if ( has_post_thumbnail() ) : ?>
  	<a href="<?php the_permalink();?>" title="<?php the_title_attribute(); ?>">
  		<?php the_post_thumbnail('full', array('class' => 'img-responsive')) ?>
  	</a>
  <?php endif; ?>
  </div>
  <div class="name_img">
    
    	<?php 
    		echo '<a href="' . get_the_permalink() . '"><h4>' . get_the_title() . ' </h4></a>'
    	?>
  </div>
</div>


<?php
// 
#======================================
# index.php
#
# The theme header.
#======================================
// 
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php wp_title('|', true, 'right'); ?><?php bloginfo('name'); ?></title>

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?> >

	<?php 
		$headerlogo = IMAGES . '/logo-small.png';
	?>

	<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>
	<div class="main_content">
		
		<!-- Header_area -->
		<div class="single_header_area">
			<div class="single_header_area_wrapper">
				<div class="logo_wrapper">
			        <a class="small_logo" href="http://ivanmartac.comxa.com/wordpress">
			          <img src="<?php echo $headerlogo; ?>" alt="">
			        </a>
			    </div>
				
				<nav id="single-navigation">
					
				    <?php
				    	wp_nav_menu(array(
				    		'menu_class' => 'single_main_nav',
				    		'theme_location' => 'main_menu',
				    		'container' => false
				    	));
				    ?>
				</nav>
	    	</div>
		</div>
